#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <arpa/inet.h> 
#include <sys/stat.h>
#include <fcntl.h>


int PORT;
char *WEBROOT = NULL;


// return whole post data  
char* get_post_data(const char *http_request) {
    
    const char *post_data_start = strstr(http_request, "\r\n\r\n"); // POST verisi iki ardışık satır sonunda başlar
    if (post_data_start != NULL) {
        post_data_start += 4; // "\r\n\r\n" sonrasını işaret eder

        // POST veri dizisini döndür
        return strdup(post_data_start); // strdup kullanarak yeni bir bellek alanı oluşturarak ve içeriği kopyalayarak döndürüyoruz
    }

    // Eğer POST veri dizisi bulunamazsa NULL döndürün
    return NULL;
}
// return user agent information



char* get_user_agent(const char *http_request) {
    // User-Agent başlığını arayın
    const char *user_agent_start = strstr(http_request, "User-Agent: ");
    if (user_agent_start != NULL) {
        // "User-Agent: " başlığının uzunluğunu hesaplayın
        user_agent_start += strlen("User-Agent: ");

        // User-Agent başlığının sonunu bulun
        const char *user_agent_end = strstr(user_agent_start, "\n");
        if (user_agent_end != NULL) {
            // Başlık aralığını belirleyin
            int user_agent_length = user_agent_end - user_agent_start;

            // User-Agent bilgisini kopyalayın ve sonuna null karakter ekleyin
            char *user_agent = (char *)malloc(user_agent_length + 1);
            strncpy(user_agent, user_agent_start, user_agent_length);
            user_agent[user_agent_length] = '\0';

            // User-Agent bilgisini döndürün
            return user_agent;
        }
    }

    // Eğer User-Agent başlığı bulunamazsa NULL döndürün
    return NULL;
}


// remove get parameters
char *get_exact_path(const char *input) {

    size_t len = strlen(input);
    char *output = malloc(3 * len + 1);
    
    // Giriş dizisini ? karakterine göre böl
    char *pos = strchr(input, '?');
    if (pos != NULL) {
        // ? karakterini bulursa, dizginin bu kısmını output dizisine kopyala
        size_t length = pos - input;
        strncpy(output, input, length);
        output[length] = '\0'; // Sonlandırıcı karakteri ekle
    } else {
        // ? karakteri bulunamazsa, giriş dizisini output dizisine kopyala
        strcpy(output, input);
    }

    return output;
}



void *handle_request(void *arg) {
    printf("-------------------------------------- \n");
    int client_socket = *((int *)arg);

    char buffer[1024] = {0};
    char url_path[300];
    char host[250] = "";
    char http_method[10]; // Metodun saklanacağı dizi



    // İstemciden gelen veriyi oku
    int bytes_read = read(client_socket, buffer, sizeof(buffer) - 1);
    if (bytes_read < 0) {
        perror("read error");
        
        close(client_socket);
        pthread_exit(NULL);
    }

    // Gelen verinin uzunluğunu kontrol et
    if (bytes_read == 0) {
        fprintf(stderr, "Hata: İstemciden gelen veri yok\n");
        close(client_socket);
        pthread_exit(NULL);
    }

    // Gelen verinin sonuna null karakter ekle
    buffer[bytes_read] = '\0';

 //  printf("buffer: %s-\n", buffer);
    
    
  
    char *user_agent = get_user_agent(buffer);
    printf("user agent: %s\n", user_agent);


     // http method
    sscanf(buffer, "%s", http_method); // İlk kelimeyi al
    //printf("http method : %s\n", http_method);


    const char *keyword = "Host: ";
    char *position = strstr(buffer, keyword);
    position += 6;
    sscanf(position, "%s", host);
   //  printf("host: %s\n", host);

   

    // -------------- GET REQUEST-----------------------
    if (strcmp(http_method, "GET") == 0) {  
      
    }

    // -------------- POST REQUEST-----------------------
    else if (strcmp(http_method, "POST") == 0) {
        
        char *post_data = get_post_data(buffer);
        if (post_data != NULL) {
            printf("POST Data: %s\n", post_data);
            free(post_data); // Belleği serbest bırakın
        } 
    } 

    else {
        // Diğer istekler
        printf("izin verilmeyen metod : %s\n", http_method);
        char response[] = "HTTP/1.1 404 Not Found\r\n\r\n";
        write(client_socket, response, strlen(response));   
        close(client_socket);
        pthread_exit(NULL);

    }





    // İstek dosyasını al
    sscanf(buffer, "%*s %s", url_path);
    //printf("url: %s\n", url_path);
    if (strcmp(url_path, "/") == 0)
        strcpy(url_path, "/index.html");
    
    char *filename = get_exact_path(url_path);  // get parametrelerini sil
    
    //printf("file: %s\n", filename);





    // Dosyanın tam yolu
    char full_path[512];
    snprintf(full_path, sizeof(full_path), "%s%s", WEBROOT, filename);
  //  printf("file path: %s\n", full_path);


  


    // Dosya aç
    int fd = open(full_path, O_RDONLY);


    if (fd == -1) {
       
        // Dosya bulunamazsa default index tanimla

        char index_file[250];
        snprintf(index_file, sizeof(index_file), "%s%s", WEBROOT, "/index.html");
      
        fd = open(index_file, O_RDONLY);

        if (fd == -1) { // index de yoksa
        
            char response[] = "HTTP/1.1 404 Not Found\r\n\r\n";
            write(client_socket, response, strlen(response));
        
            close(client_socket);
            pthread_exit(NULL);
   
        }

    }
  
    
   
    
    // Dosya oku ve istemciye gönder
    char response[1024];
    snprintf(response, sizeof(response), "HTTP/1.1 200 OK\r\nContent-Length: %d\r\n\r\n", (int)lseek(fd, 0, SEEK_END));
    write(client_socket, response, strlen(response));
    lseek(fd, 0, SEEK_SET);
    //int bytes_read;
    while ((bytes_read = read(fd, buffer, sizeof(buffer))) > 0) {
        write(client_socket, buffer, bytes_read);
    }
    close(fd);
    
    
    close(client_socket);

    pthread_exit(NULL);
}



int main(int argc, char *argv[]) {
    // stdout'un tamponunu devre dışı bırak
    setbuf(stdout, NULL);

  
    if (argc != 3 ) {
        printf("Usage : %s  <html_path> <port>\n", argv[0]);
        return 0;
    }

 
    WEBROOT = argv[1];
    PORT = atoi(argv[2]);
    


    int server_socket, client_socket;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_len = sizeof(client_addr);
    pthread_t tid;

    // Create socket
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket < 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

     // SO_REUSEADDR seçeneğini ayarla
    int opt = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    // Bind address and port
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);
    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for connections
    if (listen(server_socket, 5) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d\n", PORT);

    // Accept connections and handle requests in separate threads
    while (1) {
        client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &client_len);
        if (client_socket < 0) {
            perror("Accept failed");
            continue;
        }
       // printf("Client connected: %s:%d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

        // Create a new thread to handle the request
        if (pthread_create(&tid, NULL, handle_request, (void *)&client_socket) != 0) {
            perror("Thread creation failed");
            close(client_socket);
            continue;
        }
    }

    close(server_socket);
    return 0;
}
